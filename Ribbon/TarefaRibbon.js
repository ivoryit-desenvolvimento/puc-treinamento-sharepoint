﻿// Espera o arquivo sp.ribbon.js ser carregado.
ExecuteOrDelayUntilScriptLoaded(function () { 
	var pm = SP.Ribbon.PageManager.get_instance();

	// Adiciona evento que espera a ribbon ser iniciada.	
	pm.add_ribbonInited(function() {
		var ribbon = (SP.Ribbon.PageManager.get_instance()).get_ribbon();
	    criarAbaTarefas(ribbon);
    });
}, 'sp.ribbon.js');

function criarAbaTarefas(ribbon) {
	// Cria uma nova aba
	var tabId = 'Tab.Tarefas';
    var tabTitulo = 'Tarefas';
    var tabDescricao = 'Gerenciar tarefas';
	var tab = new CUI.Tab(ribbon, tabId, tabTitulo, tabDescricao, 'Tab.Tarefas.Command', false, '', null, null);
    ribbon.addChild(tab);

	// Cria um novo grupo dentro da aba
    var tabGroupId = 'Tab.Tarefas.Group.Gerenciar';
    var tabGroupTitulo = 'Gerenciar';
    var tabGroupDescricao = 'Gerenciar tarefas';
    var group = new CUI.Group(ribbon, tabGroupId, tabGroupTitulo, tabGroupDescricao, 'Tab.Tarefas.Group.Gerenciar.Command', null);
    tab.addChild(group);

	// Cria um novo layout
	var layout = new CUI.Layout(ribbon, 'Tab.Tarefas.Layout', 'Tab.Tarefas.Layout');
    group.addChild(layout);
    var section = new CUI.Section(ribbon, 'Tab.Tarefas.Section', 2, 'Top'); //2==OneRow
    layout.addChild(section);
    // Configura o grupo com o novo layout 
    group.selectLayout('Tab.Tarefas.Layout');
    
    
    // Botão Excluir selecionadas
    var btnExcluirTitulo = 'Excluir selecionadas';
    var btnExcluirDescricao = 'Exclui todas as tarefas selcionadas.';
	var btnExcluirPropriedades = new CUI.ControlProperties();
	    btnExcluirPropriedades.Command = 'Tab.Tarefas.Group.Gerenciar.Excluir.Command';
	    btnExcluirPropriedades.Id = 'Tab.Tarefas.Group.Gerenciar.Excluir';
	    btnExcluirPropriedades.TemplateAlias = 'o1';
	    btnExcluirPropriedades.ToolTipDescription = btnExcluirDescricao;
	    btnExcluirPropriedades.Image32by32 = '_layouts/15/images/DeleteFromMyLinksButton.gif';
	    btnExcluirPropriedades.ToolTipTitle = btnExcluirTitulo;
	    btnExcluirPropriedades.LabelText = btnExcluirTitulo;
    var btnExcluir = new CUI.Controls.Button(ribbon, 'Tab.Tarefas.Group.Gerenciar.Excluir.Button', btnExcluirPropriedades);
    	btnExcluir.set_enabled(true);
    var btnExcluirComponente = btnExcluir.createComponentForDisplayMode('Large');

	// Botão Finalizar selecionadas
    var btnFinalizarTitulo = 'Finalizar selecionadas';
    var btnFinalizarDescricao = 'Finalizar todas as tarefas selcionadas.';
	var btnFinaliarPropriedades = new CUI.ControlProperties();
	    btnFinaliarPropriedades.Command = 'Tab.Tarefas.Group.Gerenciar.Finalizar.Command';
	    btnFinaliarPropriedades.Id = 'Tab.Tarefas.Group.Gerenciar.Finalizar';
	    btnFinaliarPropriedades.TemplateAlias = 'o1';
	    btnFinaliarPropriedades.ToolTipDescription = btnFinalizarDescricao;
	    btnFinaliarPropriedades.Image32by32 = '_layouts/15/images/workflowstatus_completed.png';
	    btnFinaliarPropriedades.ToolTipTitle = btnFinalizarTitulo;
	    btnFinaliarPropriedades.LabelText = btnFinalizarTitulo;
    var btnFinalizar = new CUI.Controls.Button(ribbon, 'Tab.Tarefas.Group.Gerenciar.Finalizar.Button', btnFinaliarPropriedades);
    	btnFinalizar.set_enabled(true);
    var btnFinalizarComponente = btnFinalizar.createComponentForDisplayMode('Large');

    // Adiciona os botões a ribbon.
    var row1 = section.getRow(1);
    row1.addChild(btnExcluirComponente);
    row1.addChild(btnFinalizarComponente);
    
    SelectRibbonTab(tabId, false);
    btnExcluirComponente.set_enabled(true);
    btnFinalizarComponente.set_enabled(true);   
	
	document.querySelector("#Tab\\.Tarefas\\.Group\\.Gerenciar\\.Excluir\\.Button-Large").addEventListener('click', function(){
		var contexto = SP.ClientContext.get_current();
        var listaId = SP.ListOperation.Selection.getSelectedList();
        var itensSelecionados = SP.ListOperation.Selection.getSelectedItems(contexto);
 
        if (itensSelecionados.length > 0) {
	   		var lista = contexto.get_web().get_lists().getById(listaId);
	        contexto.load(lista);
 
	        for (indice in itensSelecionados) {
	            var item = lista.getItemById(parseInt(itensSelecionados[indice].id));
	            item.deleteObject();
	        }
	        
	        contexto.executeQueryAsync(
	            function () {
	                alert('Tarefas excluidas com sucesso.')
	                window.location.reload();
	            },
	            function (sender, args) {
	                console.error('Tarefa.Excluir - Erro ao excluir tarefas:' + args.get_message() + '\n' + args.get_stackTrace());
	            }
        	);
        }    
	});
	
	document.querySelector("#Tab\\.Tarefas\\.Group\\.Gerenciar\\.Finalizar\\.Button-Large").addEventListener('click', function(){
		var contexto = SP.ClientContext.get_current();
        var listaId = SP.ListOperation.Selection.getSelectedList();
        var itensSelecionados = SP.ListOperation.Selection.getSelectedItems(contexto);
 
        if (itensSelecionados.length > 0) {
	   		var lista = contexto.get_web().get_lists().getById(listaId);
	        contexto.load(lista);
 
	        for (indice in itensSelecionados) {
	            var item = lista.getItemById(parseInt(itensSelecionados[indice].id));
	            item.set_item('Status', 'Finalizada');
		        item.update();
	        }
	        
	        contexto.executeQueryAsync(
	            function () {
	                alert('Tarefas finalizadas com sucesso.')
	                window.location.reload();
	            },
	            function (sender, args) {
	                console.error('Tarefa.Finalizar - Erro ao finalizar tarefas:' + args.get_message() + '\n' + args.get_stackTrace());
	            }
        	);
        }    
	});
}