﻿using System;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;

namespace Ivoryit.GerenciarCursos.CursoEventReceiver
{
    /// <summary>
    /// List Item Events
    /// </summary>
    public class CursoEventReceiver : SPItemEventReceiver
    {
        /// <summary>
        /// An item is being added.
        /// </summary>
        public override void ItemAdding(SPItemEventProperties properties)
        {
            base.ItemAdding(properties);

            // Verifica se ação está sendo executada na Lista Cursos.
            if (properties.ListTitle == "Cursos")
            {
                // Assim que o curso é criada o número de vagas disponiveis é igual o número de vagas.
                var item = properties.AfterProperties;
                item["VagasDisponiveis"] = item["Vagas"];
            }
            // Verifica se ação está sendo executada na Lista CursosParticipantes.
            else if (properties.ListTitle == "CursosParticipantes")
            {
                using (var site = new SPSite(properties.Site.ID))
                {
                    using (var web = site.OpenWeb(properties.Web.ID))
                    {
                        // Obtem os cursos da nova inscrição.
                        var item = properties.AfterProperties;
                        var cursos = new SPFieldLookupValueCollection(item["Cursos"].ToString());

                        // Verifica se possui vaga disponivel para cada curso.
                        foreach (var curso in cursos)
                        {
                            // Obtem o total de inscritos.
                            var listaCursosParticipantes = web.Lists.TryGetList("CursosParticipantes");
                            var query = new SPQuery();
                            query.Query = string.Format(@"
                                    <Where>
                                        <Eq>
                                            <FieldRef Name='Cursos' LookupId='True' />
                                            <Value Type='LookupMulti'>{0}</Value>
                                        </Eq>
                                    </Where>",
                                curso.LookupId
                            );
                            var totalParticipantes = listaCursosParticipantes.GetItems(query).Count;

                            // Obtem o total de vagas.
                            var listaCursos = web.Lists.TryGetList("Cursos");
                            var totalVagas = Convert.ToInt32(listaCursos.GetItemById(curso.LookupId)["Vagas"]);

                            if (totalParticipantes == totalVagas)
                            {
                                properties.ErrorMessage += "O curso " + curso.LookupValue + " não possui vagas.";
                                properties.Status = SPEventReceiverStatus.CancelWithError;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// An item is being updated.
        /// </summary>
        public override void ItemUpdating(SPItemEventProperties properties)
        {
            base.ItemUpdating(properties);

            // Verifica se ação está sendo executada na Lista Cursos.
            if (properties.ListTitle == "Cursos")
            {
                var valorAntigo = properties.ListItem["Title"].ToString();
                var valorNovo = properties.AfterProperties["Title"].ToString();

                // Se estiver tentando mudar o título do curso, verifica se o mesmo possui inscritos.
                // Caso tenha não altera o tema do curso.
                if (valorNovo != valorAntigo)
                {
                    using (var site = new SPSite(properties.Site.ID))
                    {
                        using (var web = site.OpenWeb(properties.Web.ID))
                        {
                            // Seleciona a lista participantes.
                            var lista = web.Lists.TryGetList("CursosParticipantes");

                            // Monta a consulta que verifica se o curso que está
                            // sendo alterado já possui inscritos.
                            var query = new SPQuery();
                            query.Query = string.Format(@"
                                <Where>
                                    <Eq>
                                        <FieldRef Name='Cursos' LookupId='True' />
                                        <Value Type='LookupMulti'>{0}</Value>
                                    </Eq>
                                </Where>",
                                properties.ListItemId
                            );

                            // Caso possua inscritos não autoriza a atualização do tema do curso.
                            if (lista.GetItems(query).Count > 0)
                            {
                                properties.ErrorMessage = 
                                    "Você não pode alterar o tema do curso. " +
                                    "O mesmo já possui inscritos.";
                                properties.Status = SPEventReceiverStatus.CancelWithError;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// An item is being deleted.
        /// </summary>
        public override void ItemDeleting(SPItemEventProperties properties)
        {
            base.ItemDeleting(properties);

            // Verifica se ação está sendo executada na Lista Cursos.
            if (properties.ListTitle == "Cursos")
            {
                using (var site = new SPSite(properties.Site.ID))
                {
                    using (var web = site.OpenWeb(properties.Web.ID))
                    {
                        // Seleciona a lista CursosParticipantes
                        var lista = web.Lists.TryGetList("CursosParticipantes");

                        // Monta a consulta que verifica se o curso que está
                        // sendo excluido já possui inscritos.
                        var query = new SPQuery();
                        query.Query = string.Format(@"
                            <Where>
                                <Eq>
                                    <FieldRef Name='Cursos' LookupId='True' />
                                    <Value Type='LookupMulti'>{0}</Value>
                                </Eq>
                            </Where>",
                            properties.ListItemId
                        );

                        // Caso possua inscritos cancela a exclusão do curso.
                        if (lista.GetItems(query).Count > 0)
                        {
                            properties.ErrorMessage = "Você não pode excluir curso com inscritos.";
                            properties.Status = SPEventReceiverStatus.CancelWithError;
                        }
                    }
                }
            }
        }


    }
}