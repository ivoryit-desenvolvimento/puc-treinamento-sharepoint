using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace Ivoryit.GerenciarCursos.Features.Feature2
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("16a8091c-f014-4738-840c-4ec487dad1be")]
    public class Feature2EventReceiver : SPFeatureReceiver
    {
        private string NomeTimerJob = "Ivoryit.Cursos";

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPSite site = properties.Feature.Parent as SPSite;

            // Remove o job antes de reinstalar.
            foreach (var job in site.WebApplication.JobDefinitions)
            {
                if (job.Name == NomeTimerJob)
                {
                    job.Delete();
                    break;
                }
            }

            // Instala o job.
            var cursoVagasJob = new CursoTimerJob(
                NomeTimerJob, site.WebApplication,
                SPServer.Local, SPJobLockType.Job
            );

            // Define o intervalo de execu��o.
            var intervalo = new SPMinuteSchedule();
            intervalo.BeginSecond = 0;
            intervalo.EndSecond = 59;
            intervalo.Interval = 1;

            // Atualiza o job com as configura��es do intervalo.
            cursoVagasJob.Schedule = intervalo;
            cursoVagasJob.Update();
        }

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            SPSite site = properties.Feature.Parent as SPSite;

            // Ao desabilitar a feature o job � removido.
            foreach (var job in site.WebApplication.JobDefinitions)
            {
                if (job.Name == NomeTimerJob)
                {
                    job.Delete();
                    break;
                }
            }
        }
    }
}
