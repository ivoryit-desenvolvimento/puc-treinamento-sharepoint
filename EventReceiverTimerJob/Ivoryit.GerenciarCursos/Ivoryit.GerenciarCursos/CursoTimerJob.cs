﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivoryit.GerenciarCursos
{
    class CursoTimerJob : SPJobDefinition
    {
        public CursoTimerJob() 
        { 
        }

        public CursoTimerJob(
            string name, SPWebApplication webApp,
            SPServer server, SPJobLockType lockType) : base(name, webApp, server, lockType) 
        { 
            
        }

        public override void Execute(Guid targetInstanceId)
        {
            using (var site = new SPSite("http://treinamento"))
            {
                using (var web = site.RootWeb)
                {
                    // Obtem os cursos.
                    var listaCursos = web.Lists.TryGetList("Cursos");
                    var cursos = listaCursos.GetItems();

                    // Verifica se possui vaga disponivel nos cursos.
                    foreach (SPListItem curso in cursos)
                    {
                        // Total de vagas.
                        var totalVagas = Convert.ToInt32(curso["Vagas"]);

                        // Obtem o total de inscritos.
                        var listaCursosParticipantes = web.Lists.TryGetList("CursosParticipantes");
                        var query = new SPQuery();
                            query.Query = string.Format(@"
                                <Where>
                                    <Eq>
                                        <FieldRef Name='Cursos' LookupId='True' />
                                        <Value Type='LookupMulti'>{0}</Value>
                                    </Eq>
                                </Where>",
                                curso.ID
                            );
                            var totalInscritos = listaCursosParticipantes.GetItems(query).Count;

                        // Atualiza a quantidade de vagas disponiveis.
                        curso["VagasDisponiveis"] = totalVagas - totalInscritos;
                        curso.Update();
                    }
                }
            }
        }
    }
}
