Links

1 - CamlDesigner 2013:
    -> http://camldesigner.blob.core.windows.net/offlinepackage/CamlDesigner2013.zip

2 - CamlJs Builder:
    -> https://chrome.google.com/webstore/detail/camljs-console/ohjcpmdjfihchfhkmimcbklhjdphoeac
    -> https://github.com/andrei-markeev/camljs

Comandos power shell configuracao serviço
$account = Get-SPManagedAccount sp_farm

$appPoolSubSvc = New-SPServiceApplicationPool -Name SettingsServiceAppPool -Account $account

$appSubSvc = New-SPSubscriptionSettingsServiceApplication -ApplicationPool $appPoolSubSvc -Name SettingsServiceApp


$proxySubSvc = New-SPSubscriptionSettingsServiceApplicationProxy -ServiceApplication $appSubSvc



Loopback

New-ItemProperty HKLM:\System\CurrentControlSet\Control\Lsa -Name "DisableLoopbackCheck" -value "1" -PropertyType dword
