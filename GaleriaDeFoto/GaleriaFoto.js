﻿function GaleriaFoto() {

    this.SPContexto = new SP.ClientContext.get_current();
    this.SPLista = this.SPContexto.get_web().get_lists().getByTitle('GaleriaTeste');


    /**
     * Obtem as galerias de fotos.
     * @access Public
     * @param function callback - Função a ser executada após obter os dados.
     * @return Void
     */
    this.ObterGalerias = function (callback) {
    	var query = new CamlBuilder()
			.View(['Title', 'Descricao', 'Data1', 'FileLeafRef'])
			.Query().Where()
				.TextField('ContentType').EqualTo('Galeria')
			.ToString()
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml(query);
        
        var galerias = this.SPLista.getItems(camlQuery);

        this.SPContexto.load(galerias);
        this.SPContexto.executeQueryAsync(
            function () {
                callback(galerias);
            },
            function (sender, args) {
                console.error('GaleriaFoto.ObterGalerias - Erro ao obter galerias: ' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }
    
    /**
     * Obtem as fotos da galeria.
     * @access Public
     * @param function callback - Função a ser executada após obter os dados.
     * @return Void
     */
    this.ObterFotos = function (galeriaReferencia, callback) {
    	var query = new CamlBuilder()
			.View(['Title', 'FileRef', 'FileDirRef'])
			.Scope(CamlBuilder.ViewScope.RecursiveAll)
			.Query().Where()
				.TextField('ContentType').EqualTo('Foto')
				.And()
				.TextField('FileDirRef').EqualTo(galeriaReferencia)
			.ToString()
        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml(query);
        
        var fotos = this.SPLista.getItems(camlQuery);

        this.SPContexto.load(fotos);
        this.SPContexto.executeQueryAsync(
            function () {
                callback(fotos);
            },
            function (sender, args) {
                console.error('GaleriaFoto.ObterFotos - Erro ao obter fotos: ' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }
}


SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function() {
	var galeriaFoto = new GaleriaFoto();

    // Obtem todas as galerias
    galeriaFoto.ObterGalerias(function(galerias) {

        if (galerias.get_count() > 0) {
            var listItemEnumerator = galerias.getEnumerator();
            
            while (listItemEnumerator.moveNext()) {
                var galeria = listItemEnumerator.get_current();

				$('#galeria-foto').append(
					'<div>' +
						'<h3>' + galeria.get_item('Title') + ' - ' + galeria.get_item('Data1') + '</h3>' +
						'<p>' + galeria.get_item('Descricao') + '</p>' +
						'<p data-galeria-ref="' + galeria.get_item('FileRef') + '"></p>' + 
					'</div>');

				// Obtem todas as fotos da galeria.
				galeriaFoto.ObterFotos(galeria.get_item('FileRef'), function(fotos) {
					var listItemEnumerator = fotos.getEnumerator();
					
		            while (listItemEnumerator.moveNext()) {
		                var foto = listItemEnumerator.get_current();
		                
		                $('p[data-galeria-ref="' + foto.get_item('FileDirRef') + '"]').append(
							'<a href="' + foto.get_item('FileRef') + '" data-fancybox="' + foto.get_item('FileDirRef') + '">' +
								'<img src="' + foto.get_item('FileRef') + '" style="width:200px;" />' +
							'</a>');
		            }
				});
            }
        }
    });
});