﻿using System.Web;
using System.Web.Mvc;

namespace Ivoryit.Tarefas
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            var authorizeAttribute = new AuthorizeAttribute
            {
                // Roles = @"DOMINIO\GRUPO, DOMINIO\USUARIO..."
                Roles = @"sp\GerenciadorDeTarefas, sp\sp_admin", 
            };
            filters.Add(authorizeAttribute);
        }
    }
}
