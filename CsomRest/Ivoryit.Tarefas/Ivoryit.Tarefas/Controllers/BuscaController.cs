﻿using Ivoryit.Tarefas.Models;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Search.Query;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security;
using System.Web;
using System.Web.Mvc;

namespace Ivoryit.Tarefas.Controllers
{
    [AllowAnonymous]
    public class BuscaController : Controller
    {
        // GET: Busca
        public ActionResult Index()
        {
            var pesquisa = Request.QueryString["pesquisa"];
            var itensPorPagina = 2;
            var pagina = ObterPaginaAtual();
            var posicao = (pagina * itensPorPagina) - itensPorPagina;


            if (!string.IsNullOrEmpty(pesquisa))
            {
                #region CSOM
                //var contexto = new ClientContext("http://treinamento");
                
                //// Monta a consulta
                //var keywordQuery = new KeywordQuery(contexto);
                //keywordQuery.QueryText = pesquisa;
                //keywordQuery.RowsPerPage = itensPorPagina;
                //keywordQuery.RowLimit = itensPorPagina;
                //keywordQuery.StartRow = posicao;

                //// Realiza a consulta
                //var searchExecutor = new SearchExecutor(contexto);
                //var resultado = searchExecutor.ExecuteQuery(keywordQuery);
                //contexto.ExecuteQuery();

                //// Páginação Total de páginas.
                //decimal totalDePaginas = resultado.Value[0].TotalRows / keywordQuery.RowLimit;
                //totalDePaginas = Math.Ceiling(totalDePaginas);
                //ViewBag.TotalDePaginas = totalDePaginas;

                //// Monta model Pesquisa
                //var pesquisas = new List<BuscaModel>();
                //foreach (var linha in resultado.Value[0].ResultRows)
                //{
                //    var titulo = linha["Title"].ToString();
                //    var caminho = linha["Path"].ToString();
                //    var criado = linha["Write"] != null ? linha["Write"].ToString() : "";

                //    pesquisas.Add(new BuscaModel()
                //    {
                //        Titulo = titulo,
                //        Caminho = caminho,
                //        Criado = criado
                //    });
                //}
                //return View(pesquisas);
                #endregion

                #region REST
                var requisicao = new WebClient();
                // Desabilita autenticação AUTH. Para utilizarmos do windows.
                requisicao.Headers.Add("X-FORMS_BASED_AUTH_ACCEPTED", "f");
                // Credencial windows.
                requisicao.Credentials = new NetworkCredential("sp_farm", "senha@2018", "sp");
                // Formato dos dados que estamos enviando.
                requisicao.Headers.Add(HttpRequestHeader.ContentType, "application/json;odata=verbose");
                // Formato dos dados retornados.
                requisicao.Headers.Add(HttpRequestHeader.Accept, "application/json;odata=verbose");

                var requisicaoUrl = string.Format(
                    "http://treinamento/_api/search/query?" +
                    "querytext='{0}'&" +
                    "rowsperpage={1}&" +
                    "rowlimit={2}&" +
                    "startrow={3}",
                    pesquisa, itensPorPagina, itensPorPagina, posicao
                );


                var requisicaoResposta = requisicao.DownloadString(requisicaoUrl);
                var requisicaoRespostaParse = JObject.Parse(requisicaoResposta);
                var linhas = (JArray)requisicaoRespostaParse
                    ["d"]["query"]["PrimaryQueryResult"]["RelevantResults"]
                    ["Table"]["Rows"]["results"];

                // Páginação Total de páginas.
                var resultado = JObject.Parse(
                    requisicaoRespostaParse["d"]["query"]["PrimaryQueryResult"]["RelevantResults"].ToString()
                );
                decimal totalDePaginas = Convert.ToInt32(resultado["TotalRows"]) / itensPorPagina;
                totalDePaginas = Math.Ceiling(totalDePaginas);
                ViewBag.TotalDePaginas = totalDePaginas;

                var pesquisas = new List<BuscaModel>();
                foreach (var linha in linhas)
                {
                    var colunas = (JArray)linha["Cells"]["results"];
                    var titulo = "";
                    var caminho = "";
                    var criado = "";
                    foreach (var coluna in colunas)
                    {
                        switch (coluna["Key"].ToString())
                        {
                            case "Title":
                                titulo = coluna["Value"].ToString();
                                break;
                            case "Path":
                                caminho = coluna["Value"].ToString();
                                break;
                            case "Write":
                                criado = coluna["Value"].ToString();
                                break;
                        }
                    }

                    pesquisas.Add(new BuscaModel()
                    {
                        Titulo = titulo,
                        Caminho = caminho,
                        Criado = criado
                    });
                }

                return View(pesquisas);
                #endregion
            }

            return View();
        }

        private int ObterPaginaAtual()
        {
            try
            {
                var pagina = Request.QueryString["pagina"];
                var paginaAtual = string.IsNullOrEmpty(pagina) || pagina == "0"
                    ? 1
                    : Convert.ToInt32(pagina);
                return paginaAtual;
            }
            catch
            {
                return 1;
            }
        }
    }
}