﻿using Ivoryit.Tarefas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Ivoryit.Tarefas.Controllers
{
    [Authorize]
    public class TarefaController : Controller
    {
        public ActionResult Index()
        {
            // CSOM
            var tarefaCsom = new TarefaCSOM();
            var tarefas = tarefaCsom.Listar();
            return View(tarefas);

            // REST
            //var tarefaRest = new TarefaREST();
            //var tarefas = tarefaRest.Listar();
            //return View(tarefas);
        }

        public ActionResult Cadastrar()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Cadastrar(TarefaModel tarefa)
        {
            // CSOM
            var tarefaCsom = new TarefaCSOM();
            tarefaCsom.Cadastrar(tarefa);
            return RedirectToAction("Index");

            // REST
            //var tarefaRest = new TarefaREST();
            //tarefaRest.Cadastrar(tarefa);
            //return RedirectToAction("Index");
        }

        public ActionResult Finalizar(int id)
        {
            // CSOM
            var tarefaCsom = new TarefaCSOM();
            tarefaCsom.Finalizar(id);
            return RedirectToAction("Index");

            // REST
            //var tarefaRest = new TarefaREST();
            //tarefaRest.Finalizar(id);
            //return RedirectToAction("Index");
        }

        public ActionResult Excluir(int id)
        {
            // CSOM
            var tarefaCsom = new TarefaCSOM();
            tarefaCsom.Excluir(id);
            return RedirectToAction("Index");

            // REST
            //var tarefaRest = new TarefaREST();
            //tarefaRest.Excluir(id);
            //return RedirectToAction("Index");
        }

        public ActionResult RelatorioTarefasAbertas()
        {
            // CSOM
            var tarefaCsom = new TarefaCSOM();
            var tarefas = tarefaCsom.ObterTarefasAbertas();
            return View(tarefas);

            // REST
            //var tarefaRest = new TarefaREST();
            //var tarefas = tarefaRest.ObterTarefasAbertas();
            //return View(tarefas);
        }

        public ActionResult Forum()
        {
            // CSOM
            var tarefaForumCsom = new TarefaForumCSOM();
            ViewBag.Mensagens = tarefaForumCsom.Listar();

            return View();
        }

        [HttpPost]
        public ActionResult Forum(TarefaForumModel tarefaForum)
        {
            // CSOM
            var tarefaForumCsom = new TarefaForumCSOM();
            tarefaForumCsom.Cadastrar(tarefaForum);
            return RedirectToAction("Forum");
        }
    }
}