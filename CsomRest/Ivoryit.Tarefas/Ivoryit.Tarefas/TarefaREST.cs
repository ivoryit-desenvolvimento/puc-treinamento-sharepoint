﻿using Ivoryit.Tarefas.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Ivoryit.Tarefas
{
    public class TarefaREST
    {
        private string ApiUrl;

        public TarefaREST()
        {
            // Url base da api do SharePoint.
            ApiUrl = "http://treinamento/_api";
        }

        private WebClient CriarRequisicao()
        {
            var requisicao = new WebClient();
            // Desabilita autenticação AUTH. Para utilizarmos do windows.
            requisicao.Headers.Add("X-FORMS_BASED_AUTH_ACCEPTED", "f");
            // Credencial windows.
            requisicao.Credentials = new NetworkCredential("sp_farm", "senha@2018", "sp");
            // Formato dos dados que estamos enviando.
            requisicao.Headers.Add(HttpRequestHeader.ContentType, "application/json;odata=verbose");
            // Formato dos dados retornados.
            requisicao.Headers.Add(HttpRequestHeader.Accept, "application/json;odata=verbose");

            return requisicao;
        }

        /// <summary>
        /// Para as requisições de insert, update e delete. E necessário passar o hash formdigest.
        /// Quando estamos em uma página do SharePoint por exemplo de edição de um item.
        /// Quando entramos na página é gerado um hash. Que o SharePoint espera receber de volta 
        /// quando salvarmos os dados. Impedindo que a alteração não seja feita em outro local.
        /// </summary>
        /// <returns></returns>
        private string ObterFormDigest()
        {
            var requisicao = CriarRequisicao();
            var requisicaoUrl = ApiUrl + "/contextinfo";
            var requisicaoResposta = requisicao.UploadString(requisicaoUrl, "POST");

            var resposta = JToken.Parse(requisicaoResposta);
            return resposta["d"]["GetContextWebInformation"]["FormDigestValue"].ToString();
        }

        public List<TarefaModel> Listar()
        {
            var requisicao = CriarRequisicao();
            var requisicaoUrl = ApiUrl + "/web/lists/getbytitle('Tarefas')/Items?$select=ID,Title,Status";
            var requisicaoResposta = requisicao.DownloadString(requisicaoUrl);
            var requisicaoRespostaParse = JObject.Parse(requisicaoResposta);
            var tarefas = (JArray)requisicaoRespostaParse["d"]["results"];

            var tarefasLista = new List<TarefaModel>();
            foreach (var tarefa in tarefas)
            {
                tarefasLista.Add(new TarefaModel()
                {
                    Id = Convert.ToInt32(tarefa["ID"]),
                    Titulo = tarefa["Title"].ToString(),
                    Status = tarefa["Status"].ToString()
                });
            }
            return tarefasLista;
        }

        public List<TarefaAbertaModel> ObterTarefasAbertas()
        {
            var requisicao = CriarRequisicao();
            var requisicaoUrl = ApiUrl + "/web/lists/getbytitle('Tarefas')/Items?"+
                "$select=ID,Title,Status,Responsavel/Title,Responsavel/Email"+
                "&$expand=Responsavel" + 
                "&$orderby=Title"+
                "&$filter=Status eq 'Aberta'";
            var requisicaoResposta = requisicao.DownloadString(requisicaoUrl);
            var requisicaoRespostaParse = JObject.Parse(requisicaoResposta);
            var tarefas = (JArray)requisicaoRespostaParse["d"]["results"];

            var tarefasLista = new List<TarefaAbertaModel>();
            foreach (var tarefa in tarefas)
            {
                var responsavel = JObject.Parse(tarefa["Responsavel"].ToString());

                tarefasLista.Add(new TarefaAbertaModel()
                {
                    Id = Convert.ToInt32(tarefa["ID"]),
                    Titulo = tarefa["Title"].ToString(),
                    Status = tarefa["Status"].ToString(),
                    Responsavel = responsavel["Title"].ToString(),
                    ResponsavelEmail = responsavel["Email"].ToString()
                });
            }
            return tarefasLista;
        }

        public void Cadastrar(TarefaModel tarefa)
        {
            var formDigest = ObterFormDigest();

            var requisicao = CriarRequisicao();
            var requisicaoUrl = ApiUrl + "/web/lists/getbytitle('Tarefas')/Items";
            var requisicaoCorpo = new
            {
                __metadata = new { type = "SP.Data.TarefasListItem" },
                Title = tarefa.Titulo
            };
            requisicao.Headers.Add("X-RequestDigest", formDigest);
            requisicao.UploadString(requisicaoUrl, "POST", JsonConvert.SerializeObject(requisicaoCorpo));
        }

        public void Finalizar(int id)
        {
            var formDigest = ObterFormDigest();

            var requisicao = CriarRequisicao();
            var requisicaoUrl = ApiUrl + "/web/lists/getbytitle('Tarefas')/Items(" + id + ")";
            var requisicaoCorpo = new
            {
                __metadata = new { type = "SP.Data.TarefasListItem" },
                Status = "Finalizada"
            };
            requisicao.Headers.Add("X-RequestDigest", formDigest);
            requisicao.Headers.Add("X-HTTP-Method", "MERGE");
            requisicao.Headers.Add("If-Match", "*");
            requisicao.UploadString(requisicaoUrl, "POST", JsonConvert.SerializeObject(requisicaoCorpo));
        }

        public void Excluir(int id)
        {
            var formDigest = ObterFormDigest();

            var requisicao = CriarRequisicao();
            var requisicaoUrl = ApiUrl + "/web/lists/getbytitle('Tarefas')/Items(" + id + ")";
            requisicao.Headers.Add("X-RequestDigest", formDigest);
            requisicao.Headers.Add("X-HTTP-Method", "DELETE");
            requisicao.Headers.Add("If-Match", "*");
            requisicao.UploadString(requisicaoUrl, "POST");
        }
    }
}