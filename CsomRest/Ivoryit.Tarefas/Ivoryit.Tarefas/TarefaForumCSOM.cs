﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SharePoint.Client;
using Ivoryit.Tarefas.Models;
using System.Security;

namespace Ivoryit.Tarefas
{
    public class TarefaForumCSOM
    {
        private ClientContext Contexto;
        private List Lista;

        public TarefaForumCSOM()
        {
            // Define qual site iremos trabalhar.
            Contexto = new ClientContext("https://ivoryit.sharepoint.com/sites/treinamento-puc");

            // Credêncial SharePoint Online.
            var senha = new SecureString();
            foreach (char c in "puc@2018")
            {
                senha.AppendChar(c);
            }
            Contexto.Credentials = new SharePointOnlineCredentials(
                "treinamento.puc@ivoryit.onmicrosoft.com", 
                senha);

            // Define qual lista iremos trabalhar.
            Lista = Contexto.Web.Lists.GetByTitle("ForumMensagens");
        }

        public List<TarefaForumModel> Listar()
        {
            var query = new CamlQuery();
            query.ViewXml= @"
                <View>
                    <Query>
                        <Where>
                            <Eq>
                                <FieldRef Name='Aprovado' />
                                <Value Type='Integer'>1</Value>
                            </Eq>
                        </Where>
                        <OrderBy>
                            <FieldRef Name='Created' />
                        </OrderBy>
                    </Query>
                </View>";
            var mensagens = Lista.GetItems(query);

            Contexto.Load(mensagens);
            Contexto.ExecuteQuery();

            var mensagensLista = new List<TarefaForumModel>();
            foreach (var mensagem in mensagens)
            {
                mensagensLista.Add(new TarefaForumModel()
                {
                    Id = mensagem.Id,
                    Nome = mensagem["Title"].ToString(),
                    Mensagem = mensagem["Mensagem"].ToString()
                });
            }
            return mensagensLista;
        }

        public void Cadastrar(TarefaForumModel mensagem)
        {
            var novoItem = new ListItemCreationInformation();
            var novaMensagem = Lista.AddItem(novoItem);
            novaMensagem["Title"] = mensagem.Nome;
            novaMensagem["Mensagem"] = mensagem.Mensagem;
            novaMensagem.Update();

            Contexto.ExecuteQuery();
        }
    }
}