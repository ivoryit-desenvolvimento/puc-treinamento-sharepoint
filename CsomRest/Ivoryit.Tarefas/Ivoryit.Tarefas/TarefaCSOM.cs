﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SharePoint.Client;
using Ivoryit.Tarefas.Models;
using System.Security;

namespace Ivoryit.Tarefas
{
    public class TarefaCSOM
    {
        private ClientContext Contexto;
        private List Lista;

        public TarefaCSOM()
        {
            // Define qual site iremos trabalhar.
            Contexto = new ClientContext("http://treinamento");
            
            // Por padrão a aplicação irá tentar utilizar a credencial do usuário 
            // que está rodando a aplicação. Para utilizar uma credencial com permissão do site
            // basta informa a mesma da seguinte maneira para o contexto:
            //Contexto.Credentials = new NetworkCredential("sp_farm", "senha@2018", "sp");

            // Define qual lista iremos trabalhar.
            Lista = Contexto.Web.Lists.GetByTitle("Tarefas");
        }

        public List<TarefaModel> Listar()
        {
            var query = CamlQuery.CreateAllItemsQuery();
            var tarefas = Lista.GetItems(query);

            Contexto.Load(tarefas);
            Contexto.ExecuteQuery();

            var tarefasLista = new List<TarefaModel>();
            foreach (var tarefa in tarefas)
            {
                tarefasLista.Add(new TarefaModel()
                {
                    Id = tarefa.Id,
                    Titulo = tarefa["Title"].ToString(),
                    Status = tarefa["Status"].ToString()
                });
            }
            return tarefasLista;
        }

        public void Cadastrar(TarefaModel tarefa)
        {
            var novoItem = new ListItemCreationInformation();
            var novaTarefa = Lista.AddItem(novoItem);
            novaTarefa["Title"] = tarefa.Titulo;
            novaTarefa.Update();

            Contexto.ExecuteQuery();
        }

        public void Finalizar(int id)
        {
            var tarefa = Lista.GetItemById(id);
            tarefa["Status"] = "Finalizada";
            tarefa.Update();

            Contexto.ExecuteQuery();
        }

        public void Excluir(int id)
        {
            var tarefa = Lista.GetItemById(id);
            tarefa.DeleteObject();

            Contexto.ExecuteQuery();
        }

        public List<TarefaAbertaModel> ObterTarefasAbertas()
        {
            var query = new CamlQuery();
            query.ViewXml = @"
                <View>
                    <ViewFields>
                        <FieldRef Name='Title' />
                        <FieldRef Name='Status' />
                        <FieldRef Name='Responsavel' />
                        <FieldRef Name='ResponsavelEmail' />
                    </ViewFields>
                    <Joins>
                        <Join Type='LEFT' ListAlias='TarefasResponsaveis'>
                            <Eq>
                                <FieldRef Name='Responsavel' RefType='ID' />
                                <FieldRef Name='ID' List='TarefasResponsaveis' />
                            </Eq>
                        </Join>
                    </Joins>
                    <ProjectedFields>
                        <Field ShowField='Email' Type='Lookup' Name='ResponsavelEmail' 
                            List='TarefasResponsaveis' />
                    </ProjectedFields>
                    <Query>
                        <OrderBy>
                            <FieldRef Name='Title' />
                        </OrderBy>
                        <Where>
                            <Eq>
                                <FieldRef Name='Status' />
                                <Value Type='Choice'>Aberta</Value>
                            </Eq>
                        </Where>
                    </Query>
                </View>";
            var tarefas = Lista.GetItems(query);

            Contexto.Load(tarefas);
            Contexto.ExecuteQuery();

            var tarefasLista = new List<TarefaAbertaModel>();
            foreach (var tarefa in tarefas)
            {
                var responsavelLookup = (FieldLookupValue)tarefa["Responsavel"];
                var responsavel = responsavelLookup != null ? responsavelLookup.LookupValue : "";

                var responsavelEmailLookup = (FieldLookupValue)tarefa["ResponsavelEmail"];
                var responsavelEmail = responsavelEmailLookup != null ? responsavelEmailLookup.LookupValue : "";

                tarefasLista.Add(new TarefaAbertaModel()
                {
                    Id = tarefa.Id,
                    Titulo = tarefa["Title"].ToString(),
                    Status = tarefa["Status"].ToString(),
                    Responsavel = responsavel,
                    ResponsavelEmail = responsavelEmail
                });
            }
            return tarefasLista;
        }
    }
}