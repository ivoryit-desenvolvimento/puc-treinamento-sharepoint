﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivoryit.Tarefas.Models
{
    public class BuscaModel
    {
        public string Titulo { get; set; }

        public string Caminho { get; set; }

        public string Criado { get; set; }
    }
}
