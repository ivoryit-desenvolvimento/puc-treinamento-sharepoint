﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivoryit.Tarefas.Models
{
    public class TarefaAbertaModel
    {
        public int Id { get; set; }

        public string Titulo { get; set; }

        public string Status { get; set; }

        public string Responsavel { get; set; }

        public string ResponsavelEmail { get; set; }
    }
}
