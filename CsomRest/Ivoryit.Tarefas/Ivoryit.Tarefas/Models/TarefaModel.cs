﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ivoryit.Tarefas.Models
{
    public class TarefaModel
    {
        public int Id { get; set; }

        [Required]
        public string Titulo { get; set; }

        public string Status { get; set; }
    }
}