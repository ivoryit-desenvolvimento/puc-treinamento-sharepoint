﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.SharePoint.Client;
using System.Security;
using PUC.Forum.Models;

namespace PUC.Forum
{
    public class ForumCSOM
    {
        private ClientContext Contexto;
        private List Lista;

        public ForumCSOM()
        {
            // Define qual site iremos trabalhar.
            Contexto = new ClientContext("https://ivoryit.sharepoint.com/sites/treinamento-puc");

            var senha = new SecureString();
            foreach(var c in "******")
            {
                senha.AppendChar(c);
            }
            Contexto.Credentials = new SharePointOnlineCredentials("treinamento.puc@ivoryit.onmicrosoft.com", senha);

            // Define qual lista iremos trabalhar.
            Lista = Contexto.Web.Lists.GetByTitle("ForumMensagens");
        }

        public List<MensagemModel> Listar()
        {
            
        }

        public void Cadastrar(MensagemModel mensagem)
        {
            
        }
    }
}