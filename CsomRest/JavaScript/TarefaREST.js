﻿function TarefaREST() {

    this.ApiUrl = _spPageContextInfo.webAbsoluteUrl + '/_api';

    /**
     * Cadastrar tarefa
     * @access Public
     * @param string tarefaDescricao - Descrição da tarefa a ser cadastrada
     * @return Void
     */    
     this.Cadastrar = function (tarefaDescricao) {
        var url = this.ApiUrl + "/web/lists/getbytitle('Tarefas')/Items";
        var data = {
            '__metadata' : { 'type' : 'SP.Data.TarefasListItem' },
            'Title' : tarefaDescricao
        };

        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'accept': 'application/json;odata=verbose',
                'X-RequestDigest': $('#__REQUESTDIGEST').val(),
                'content-Type': 'application/json;odata=verbose'
            },
            data: JSON.stringify(data),
            success: function (data) {
                alert('Tarefa cadastrada com sucesso.')
                window.location.reload();
            },
            error: function (error) {
                console.error('Tarefa.Cadastrar - Erro ao cadastrar tarefa:' + JSON.stringify(error));
            }
        });
    }

    /**
     * Listar tarefas
     * @access Public
     * @param function callback - Função a ser executada após obter os dados.
     * @return Void
     */
    this.Listar = function (callback) {
        var url = this.ApiUrl + "/web/lists/getbytitle('Tarefas')/Items?$select=ID,Title,Status";
        $.ajax({
            url: url,
            type: 'GET',
            headers: {
                'accept': 'application/json;odata=verbose',
            },
            success: function (data) {
                callback(data.d.results);
            },
            error: function (error) {
                console.error('Tarefa.Listar - Erro ao obter tarefas: ' + JSON.stringify(error));
            }
        });
    }

    /**
     * Finalizar tarefa
     * @param int id - Id da tarefa a qual deseja finalizar.
     * @access Public
     * @return Void
     */
    this.Finalizar = function (id) {
        var url = this.ApiUrl + "/web/lists/getbytitle('Tarefas')/Items(" + id + ")";
        var data = {
            '__metadata' : { 'type' : 'SP.Data.TarefasListItem' },
            'Status' : 'Finalizada'
        };

        $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'accept': 'application/json;odata=verbose',
                'X-RequestDigest': $('#__REQUESTDIGEST').val(),
                'content-Type': 'application/json;odata=verbose',
                'X-Http-Method': 'MERGE',
                'If-Match': '*'
            },
            data: JSON.stringify(data),
            success: function (data) {
                alert('Tarefa finalizada com sucesso.')
                window.location.reload();
            },
            error: function (error) {
                console.error('Tarefa.Finalizar - Erro ao finalizar tarefa:' + JSON.stringify(error));
            }
        });
    }

    /**
     * Excluir tarefa
     * @param int id - Id da tarefa a qual deseja excluir.
     * @access Public
     * @return Void
     */
    this.Excluir = function (id) {
        var url = this.ApiUrl + "/web/lists/getbytitle('Tarefas')/Items(" + id + ")";

        $.ajax({
            url: url,
            type: 'DELETE',
            headers: {
                'accept': 'application/json;odata=verbose',
                'X-RequestDigest': $('#__REQUESTDIGEST').val(),
                'If-Match': '*'
            },
            success: function (data) {
                alert('Tarefa excluida com sucesso.')
                window.location.reload();
            },
            error: function (error) {
                console.error('Tarefa.Excluir - Erro ao excluir tarefa:' + JSON.stringify(error));
            }
        });
    }
}

var tarefa;

SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function() {
    tarefa = new TarefaREST();

    // Obtem todas as tarefas cadastradas.
    tarefa.Listar(function(tarefas, objetoTipo) {
    	var listaTarefas = document.querySelector('#tarefa-lista');

       	if (tarefas.length > 0) {
	        for (var i =0; i < tarefas.length; i++) {
	            listaTarefas.innerHTML += 
	                '<tr>' +
	                    '<td>' + tarefas[i].Title + '</td>' +
	                    '<td>' + tarefas[i].Status + '</td>' +
	                    '<td>' +
	                        '<button type="button" onClick="tarefa.Finalizar(' + tarefas[i].ID + ')">Finalizar</button> ' +
	                        '<button type="button" onClick="tarefa.Excluir(' + tarefas[i].ID + ')">Excluir</button>' +
	                    '</td>' +
	                '</tr>';
	        }
        } else {
            listaTarefas.innerHTML = 
                '<tr>' +
                    '<td colspan="3">Nenhuma tarefa cadastrada.</td>' +
                '</tr>';
        }
    });

    // Ação salvar tarefa.
    document.querySelector('#tarefa-salvar').addEventListener('click', function() {
        var tarefaDescricao = document.querySelector('#tarefa-titulo').value;

        if (tarefaDescricao != '') {
            tarefa.Cadastrar(tarefaDescricao);
        } else {
            alert('O campo Tarefa é obrigatório.');
        }
    });
});