﻿function TarefaCSOM() {

    this.SPContexto = new SP.ClientContext.get_current();
    this.SPLista = this.SPContexto.get_web().get_lists().getByTitle('Tarefas');


    /**
     * Cadastrar tarefa
     * @access Public
     * @param string tarefaDescricao - Descrição da tarefa a ser cadastrada
     * @return Void
     */
    this.Cadastrar = function (tarefaDescricao) {
        var tarefa = this.SPLista.addItem(new SP.ListItemCreationInformation());
        tarefa.set_item('Title', tarefaDescricao);
        tarefa.update();

        this.SPContexto.load(tarefa);
        this.SPContexto.executeQueryAsync(
            function () {
                alert('Tarefa cadastrada com sucesso.')
                window.location.reload();
            },
            function (sender, args) {
                console.error('Tarefa.Cadastrar - Erro ao cadastrar tarefa:' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }

    /**
     * Listar tarefas
     * @access Public
     * @param function callback - Função a ser executada após obter os dados.
     * @return Void
     */
    this.Listar = function (callback) {
        var query = SP.CamlQuery.createAllItemsQuery();
        var tarefas = this.SPLista.getItems(query);

        this.SPContexto.load(tarefas);
        this.SPContexto.executeQueryAsync(
            function () {
                callback(tarefas);
            },
            function (sender, args) {
                console.error('Tarefa.Listar - Erro ao obter tarefas: ' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }

    /**
     * Finalizar tarefa
     * @param int id - Id da tarefa a qual deseja finalizar.
     * @access Public
     * @return Void
     */
    this.Finalizar = function (id) {
        var tarefa = this.SPLista.getItemById(id);
        tarefa.set_item('Status', 'Finalizada');
        tarefa.update();

        this.SPContexto.executeQueryAsync(
            function () {
                alert('Tarefa finalizada com sucesso.')
                window.location.reload();
            },
            function (sender, args) {
                console.error('Tarefa.Finalizar - Erro ao finalizar tarefa:' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }

    /**
     * Excluir tarefa
     * @param int id - Id da tarefa a qual deseja excluir.
     * @access Public
     * @return Void
     */
    this.Excluir = function (id) {
        var tarefa = this.SPLista.getItemById(id);
        tarefa.deleteObject();

        this.SPContexto.executeQueryAsync(
            function () {
                alert('Tarefa excluida com sucesso.')
                window.location.reload();
            },
            function (sender, args) {
                console.error('Tarefa.Excluir - Erro ao excluir tarefa:' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }
}

var tarefa;

SP.SOD.executeFunc('sp.js', 'SP.ClientContext', function() {
	tarefa = new TarefaCSOM();
	
    // Obtem todas as tarefas cadastradas.
    tarefa.Listar(function(tarefas, objetoTipo) {
        var listaTarefas = document.querySelector('#tarefa-lista');

        if (tarefas.get_count() > 0) {
            var listItemEnumerator = tarefas.getEnumerator();
            while (listItemEnumerator.moveNext()) {
                var item = listItemEnumerator.get_current();

                listaTarefas.innerHTML += 
                    '<tr>' +
                        '<td>' + item.get_item('Title') + '</td>' +
                        '<td>' + item.get_item('Status') + '</td>' +
                        '<td>' +
                            '<button type="button" onClick="tarefa.Finalizar(' + item.get_item('ID') + ')">Finalizar</button> ' +
                            '<button type="button" onClick="tarefa.Excluir(' + item.get_item('ID') + ')">Excluir</button>' +
                        '</td>' +
                    '</tr>';
            }
        } else {
            listaTarefas.innerHTML = 
                '<tr>' +
                    '<td colspan="3">Nenhuma tarefa cadastrada.</td>' +
                '</tr>';
        }
    });

    // Ação salvar tarefa.
    document.querySelector('#tarefa-salvar').addEventListener('click', function() {
        var tarefaDescricao = document.querySelector('#tarefa-titulo').value;

        if (tarefaDescricao != '') {
            tarefa.Cadastrar(tarefaDescricao);
        } else {
            alert('O campo Tarefa é obrigatório.');
        }
    });
});