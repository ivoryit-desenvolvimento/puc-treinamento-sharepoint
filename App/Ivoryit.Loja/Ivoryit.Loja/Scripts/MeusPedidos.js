﻿'use strict';

function MeusPedidos() {

    this.SPContexto = SP.ClientContext.get_current();
    this.SPListaPedidosProdutos = this.SPContexto.get_web().get_lists().getByTitle('PedidosProdutos');

    /**
     * Função construtora.
     * @access Private
     * @return Void
     */
    this.Construtor = function () {
    }

    /**
     * Obtem todos os pedidos do usuário logado.
     * @access Private
     * @return Void
     */
    this.CarregarGrid = function () {
        // Monta query
        var query = new CamlBuilder()
	        .View(['PedidoNumero', 'ProdutoNome', 'ProdutoValor'])
	        .InnerJoin('Pedido', 'Pedidos')
		        .Select('Title', 'PedidoNumero')
	        .InnerJoin('Produto', 'Produtos')
		        .Select('Title', 'ProdutoNome')
		        .Select('Valor', 'ProdutoValor')
	        .Query().Where()
		        .UserField('Author').EqualToCurrentUser()
	        .OrderBy('PedidoNumero')
	        .ToString();

        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml(query);

        var pedidos = this.SPListaPedidosProdutos.getItems(camlQuery);
        this.SPContexto.load(pedidos);
        this.SPContexto.executeQueryAsync(
            function() {
                // Exibe os produtos encontrados na consulta.
                var listItemEnumerator = pedidos.getEnumerator();
                while (listItemEnumerator.moveNext()) {
                    var item = listItemEnumerator.get_current();

                    var linhaPedido =
                        '<tr>' +
                            '<td>' + item.get_item('PedidoNumero').get_lookupValue() + '</td>' +
                            '<td>' + item.get_item('ProdutoNome').get_lookupValue() + '</td>' +
                            '<td>' + item.get_item('ProdutoValor').get_lookupValue() + '</td>' +
                        '</tr>';

                    $('#meus-pedidos-grid').append(linhaPedido);
                }
            },
            function (sender, args) {
                console.error('MeusPedidos.CarregarGrid - Error ao obter pedidos:' + args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }


    this.Construtor();
}

$(document).ready(function () {
    var meusPedidos = new MeusPedidos()
    meusPedidos.CarregarGrid();
});