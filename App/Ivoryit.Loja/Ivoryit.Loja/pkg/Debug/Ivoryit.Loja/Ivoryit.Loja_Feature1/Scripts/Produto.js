﻿'use strict';

function Produto() {

    this.SPContexto = SP.ClientContext.get_current();
    this.SPListaProdutos = this.SPContexto.get_web().get_lists().getByTitle('Produtos');

    this.Produtos = null;

    this.PaginacaoItensPorPagina = 2;

    this.ProdutosPagina = 1;
    this.ProdutosPosicao = null;
    this.ProdutosPagAnterior = null;
    this.ProdutosPagProximo = null;


    /**
     * Função construtora.
     * @access Private
     * @return Void
     */
    this.Construtor = function () {
    }

    /**
     * Obtem todos os produtos e exibe na tabela.
     * @access Private
     * @return Void
     */
    this.CarregarGrid = function () {
        $('#produtos-grid').html('');

        // Monta query
        var query = new CamlBuilder()
	        .View(['ID', 'Title', 'Valor'])
            .RowLimit(this.PaginacaoItensPorPagina)
	        .Query().Where().All()
	        .OrderBy('Title')
	        .ToString();

        var camlQuery = new SP.CamlQuery();
        camlQuery.set_listItemCollectionPosition(this.ProdutosPosicao);
        camlQuery.set_viewXml(query);

        this.Produtos = this.SPListaProdutos.getItems(camlQuery);
        this.SPContexto.load(this.Produtos);
        this.SPContexto.executeQueryAsync(
            Function.createDelegate(this, this.CarregarGridSucesso),
            Function.createDelegate(this, this.CarregarGridErro)
        );
    }

    this.CarregarGridSucesso = function () {
        // Exibe os produtos encontrados na consulta.
        var listItemEnumerator = this.Produtos.getEnumerator();
        while (listItemEnumerator.moveNext()) {
            var item = listItemEnumerator.get_current();

            var produto =
                '<tr>' +
                    '<td>' + item.get_item('Title') + '</td>' +
                    '<td>' + item.get_item('Valor') + '</td>' +
                    '<td>' +
                        '<button class="produto-add-carrinho btn btn-success" data-id="' + item.get_item('ID') + '">' +
                            'Adicionar ao carrinho' +
                        '</button>' +
                    '</td>' +
                '</tr>';

            $('#produtos-grid').append(produto);
        }

        // Páginação
        if (this.Produtos.get_count()) {
            // Obtem o posicionamento da proxima página.
            if (this.Produtos.get_listItemCollectionPosition()) {
                this.ProdutosPagProximo = this.Produtos.get_listItemCollectionPosition().get_pagingInfo();
            } else {
                this.ProdutosPagProximo = null;
            }

            $('#produtos-paginacao .indice').html(((
                (this.ProdutosPagina - 1) * this.PaginacaoItensPorPagina) + 1) 
                + " - " + 
                (
                    (this.ProdutosPagina * this.PaginacaoItensPorPagina) - 
                    (this.PaginacaoItensPorPagina - this.Produtos.get_count())
                )
            );

            // Obtem o posicionamento da página anterior.
            this.ProdutosPagAnterior = "PagedPrev=TRUE&Paged=TRUE&p_ID=" + 
                this.Produtos.itemAt(0).get_item('ID') + 
                '&p_Title=' + this.Produtos.itemAt(0).get_item('Title');

            // Verifica se chegou ao fim de páginas e bloqueia os botões.
            if (this.ProdutosPagina <= 1) {
                $("#produtos-paginacao .anterior").attr('disabled', 'disabled');
            }
            else {
                $("#produtos-paginacao .anterior").removeAttr('disabled');
            }

            if (this.ProdutosPagProximo) {
                $("#produtos-paginacao .proximo").removeAttr('disabled');
            }
            else {
                $("#produtos-paginacao .proximo").attr('disabled', 'disabled');
            }
        }
    }

    this.CarregarGridErro = function (sender, args) {
        console.error('Produto.CarregarGrid - Error ao obter produtos:' + args.get_message() + '\n' + args.get_stackTrace());
    }

    this.CarregarGridProximaPag = function () {
        this.ProdutosPagina = this.ProdutosPagina + 1;
        if (this.ProdutosPagProximo) {
            this.ProdutosPosicao = new SP.ListItemCollectionPosition();
            this.ProdutosPosicao.set_pagingInfo(this.ProdutosPagProximo);
        } else {
            this.ProdutosPosicao = null;
        }

        this.CarregarGrid();
    }

    this.CarregarGridPagAnterior = function () {
        this.ProdutosPagina = this.ProdutosPagina - 1;
        this.ProdutosPosicao = new SP.ListItemCollectionPosition();
        this.ProdutosPosicao.set_pagingInfo(this.ProdutosPagAnterior);
        this.CarregarGrid();
    }


    this.Construtor();
}

$(document).ready(function () {
    var produto = new Produto()
    produto.CarregarGrid();

    //  Páginação ações Anterior / Próximo
    $('#produtos-paginacao .proximo').click(function () {
        produto.CarregarGridProximaPag();
    });

    $('#produtos-paginacao .anterior').click(function () {
        produto.CarregarGridPagAnterior();
    });

    // Ação adicionar ao carrinho
    $('#produtos-grid').on('click', '.produto-add-carrinho', function () {
        var carrinho = app.GetSession('carrinho');
        var produtoId = $(this).attr('data-id');
        var produtoNome = $(this).parents('tr').find('td:nth-child(1)').text();
        var produtoValor = $(this).parents('tr').find('td:nth-child(2)').text();

        if (carrinho != null && carrinho != '') {
            carrinho = JSON.parse(carrinho);
            carrinho.push({
                Id: produtoId,
                Titulo: produtoNome,
                Valor: produtoValor
            })
        } else {
            carrinho = [];
            carrinho.push({
                Id: produtoId,
                Titulo: produtoNome,
                Valor: produtoValor
            })
        }

        app.SetSession('carrinho', JSON.stringify(carrinho));
        alert('Produto adicionado ao carrinho.');
    });
});
