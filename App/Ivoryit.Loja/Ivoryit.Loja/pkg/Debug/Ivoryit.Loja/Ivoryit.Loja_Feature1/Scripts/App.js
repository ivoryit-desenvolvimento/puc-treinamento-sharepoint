﻿'use strict';

/**
 * Classe com funções para auxiliar o desenvolvimento
 */
function App() {

    /**
     * Função construtora.
     * @access Private
     * @return Void
     */
    this.Construtor = function () {
    }

    /**
     * Truncar - Realizar um corte no texto de acordo com o limite passado.
     * @access Public
     * @param String texto
     * @param Int limite
     * @return String
     */
    this.Truncar = function (texto, limite) {
        texto = texto != null ? texto : '';

        if (texto.length > limite) {
            limite--;
            var last = texto.substr(limite - 1, 1);
            while (last != ' ' && limite > 0) {
                limite--;
                last = texto.substr(limite - 1, 1);
            }

            last = texto.substr(limite - 2, 1);

            if (last == ',' || last == ';' || last == ':') {
                texto = texto.substr(0, limite - 2) + '...';
            } else if (last == '.' || last == '?' || last == '!') {
                texto = texto.substr(0, limite - 1);
            } else {
                texto = texto.substr(0, limite - 1) + '...';
            }
        }

        return texto;
    }

    this.ConverterDataUtcParaLocal = function (dataUtc) {
        var offset = new Date().getTimezoneOffset();
        var dataLocal = dataUtc.setMinutes(dataUtc.getMinutes() + offset);
        return new Date(dataLocal);
    }

    /**
     * Formata DateTime para o padrão escolhido
     * @access Public
     * @param Date data
     * @param String formato
     * @return String
     */
    this.FormatarData = function (data, formato) {
        if (data != null && data != '') {
            if (typeof data !== 'string' || data instanceof String) {
                data = data.toUTCString();
                data = new Date(data);
            } else {
                data = new Date(data);
            }
            data = this.ConverterDataUtcParaLocal(data);

            var d = data.getDate();
            d = this.ZeroEsquerda(d, 2);
            var m = data.getMonth() + 1;
            m = this.ZeroEsquerda(m, 2);
            var y = data.getFullYear();
            var h = data.getHours();
            h = this.ZeroEsquerda(h, 2);
            var mm = data.getMinutes();
            mm = this.ZeroEsquerda(mm, 2);

            switch (formato) {
                case '99/99/9999':
                    data = d + '/' + m + '/' + y;
                    break;
                case '99/99/9999 99:99':
                    data = d + '/' + m + '/' + y + ' ' + h + ':' + mm;
                    break;
                case '9999-99-99':
                    data = y + '/' + m + '/' + d;
                    break;
                case '9999-99-99 99:99':
                    data = y + '/' + m + '/' + d + ' ' + h + ':' + mm;
                    break;
            }

            return data;
        } else {
            return '';
        }
    }

    /**
     * Formata data vinda de uma lista do SharePoint para o padrão brasileiro.
     * @access Public
     * @param Date data
     * @return String
     */
    this.FormatarDataListaParaDataBr = function (data) {
        if (data != null && data != '') {
            if (typeof data !== 'string' || data instanceof String) {
                data = data.toUTCString();
            }

            if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
                data = data.replace('00:00:00', '01:00:00');
            }

            data = new Date(data);
            data = this.ConverterDataUtcParaLocal(data);

            var d = data.getDate();
            d = d <= 9 ? '0' + d : d;
            var m = data.getMonth() + 1;
            m = m <= 9 ? '0' + m : m;
            var y = data.getFullYear();
            var data = d + '/' + m + '/' + y;

            return data;
        } else {
            return '';
        }
    }

    /**
     * Formata hora vinda de uma lista do SharePoint para o padrão brasileiro.
     * @access Public
     * @param Date data
     * @return String
     */
    this.FormatarHoraListaParaHoraBr = function (data) {
        if (data != null && data != '') {
            if (typeof data !== 'string' || data instanceof String) {
                data = data.toUTCString();
            }

            if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
                data = data.replace('00:00:00', '01:00:00');
            }

            data = new Date(data);
            data = this.ConverterDataUtcParaLocal(data);

            var h = data.getHours();
            var m = data.getMinutes();

            var hora = this.ZeroEsquerda(h, 2) + ':' + this.ZeroEsquerda(m, 2);
            return hora;
        } else {
            return '';
        }
    }

    /**
     * Formata data no padrão brasileiro para o padrão de lista do SharePoint.
     * @access Public
     * @param String data
     * @return String
     */
    this.FormatarDataBrParaDataLista = function (data) {
        data = data.split('/');
        var dataLista = data[2] + '-' + data[1] + '-' + data[0];
        return dataLista;
    }

    /**
     * Formata string com zero a esquerda.
     * @access Public
     * @param String str - String a qual deseja formatar com zero a esquerda
     * @param Int max - Tamanho da string
     * @return String
     */
    this.ZeroEsquerda = function (str, max) {
        str = str.toString();
        return str.length < max ? this.ZeroEsquerda('0' + str, max) : str;
    }

    /**
     * Obtem parâmetros da url.
     * @access Public
     * @param String parametro - Qual parâmetro você quer obter da url.
     * @param mixed - Retorna o valor do parâmetro.
     */
    this.ObterUrlParametro = function (parametro) {
        parametro = parametro.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + parametro + "=([^&#]*)"),
            resultado = regex.exec(location.search);

        return resultado == null ? "" : decodeURIComponent(resultado[1].replace(/\+/g, " "));
    }

    this.ObterParametrosSharepoint = function () {
        var hostUrl = this.ObterUrlParametro('SPHostUrl');
        var language = this.ObterUrlParametro('SPLanguage');
        var clientTag = this.ObterUrlParametro('SPClientTag');
        var productNumber = this.ObterUrlParametro('SPProductNumber');
        var appWebUrl = this.ObterUrlParametro('SPAppWebUrl');

        return 'SPHostUrl=' + hostUrl + '&SPAppWebUrl=' + appWebUrl + '&SPLanguage=' + language + '&SPClientTag=' + clientTag + '&SPProductNumber=' + productNumber;
    }

    /**
     * Funções para setar informações na session.
     * @access public
     * @param string key - Chave que irá ser salvo no 
     * @return void
     */
    this.SetSession = function (key, value) {
        if (typeof (Storage) !== 'undefined') {
            sessionStorage.setItem(key, value);
        }
    }

    /**
     * Funções para obter informações na session.
     * @access public
     * @param string key - Chave que queremos obter o valor.
     * @return object mixed
     */
    this.GetSession = function (key) {
        try {
            if (typeof (Storage) !== 'undefined') {
                return sessionStorage.getItem(key);
            } else {
                return null;
            }
        }
        catch (err) {
            return null;
        }
    }


    this.Construtor();
}
var app = new App();
