﻿'use strict';

function Carrinho() {

    this.SPContexto = SP.ClientContext.get_current();
    this.SPListaPedidos = this.SPContexto.get_web().get_lists().getByTitle('Pedidos');
    this.SPListaPedidosProdutos = this.SPContexto.get_web().get_lists().getByTitle('PedidosProdutos');


    /**
     * Função construtora.
     * @access Private
     * @return Void
     */
    this.Construtor = function () {
    }

    /**
     * Obtem todos os produtos que estão no carrinho e exibe na tela.
     * @access Private
     * @return Void
     */
    this.CarregarGrid = function () {
        var carrinho = app.GetSession('carrinho');
        
        if (carrinho != null && carrinho != '') {
            carrinho = JSON.parse(carrinho);

            var total = 0;
            $.each(carrinho, function (indice, item) {
                var produto =
                    '<tr>' +
                        '<td>' + item.Titulo + '</td>' +
                        '<td>' + item.Valor + '</td>' +
                    '</tr>';
                total += parseFloat(item.Valor);

                $('#carrinho-grid').append(produto);
            });
            $('.finalizar-compra span').text(total);
        } else {
            var produto =
                '<tr>' +
                    '<td colspan="2">Carrinho vazio</td>' +
                '</tr>';
            $('.finalizar-compra').hide();
        }
    }

    /**
     * Obtem todos os produtos que estão no carrinho e gera um novo pedido
     * @access Private
     * @return Void
     */
    this.FinalizarCompra = function () {
        var carrinho = app.GetSession('carrinho');

        if (carrinho != null && carrinho != '') {
            // Primeiro cria um novo pedido. Para depois adicionar os produtos ao mesmo.
            var pedido = this.SPListaPedidos.addItem(new SP.ListItemCreationInformation());
            var pedidoNumero = new Date();
            pedido.set_item('Title', pedidoNumero.getTime());
            pedido.update();

            var contexto = this.SPContexto;
            var listaPedidosProdutos = this.SPListaPedidosProdutos;

            this.SPContexto.load(pedido);
            this.SPContexto.executeQueryAsync(
                function () {
                    carrinho = JSON.parse(carrinho);

                    var pedidoProduto = null;
                    $.each(carrinho, function (indice, item) {
                        pedidoProduto = listaPedidosProdutos.addItem(new SP.ListItemCreationInformation());
                        pedidoProduto.set_item('Pedido', pedido.get_item('ID'));
                        pedidoProduto.set_item('Produto', item.Id);
                        pedidoProduto.update();
                    });
                    contexto.load(pedidoProduto);
                    contexto.executeQueryAsync(
                        function () {
                            // Limpa o carrinho e volta para home.
                            app.SetSession('carrinho', '');
                            alert('Compra finalizada com sucesso.');
                            window.location = 'Default.aspx';
                        }
                    );
                }
            );
        }
    }


    this.Construtor();
}

$(document).ready(function () {
    var carrinho = new Carrinho()
    carrinho.CarregarGrid();

    // Ação ao clicar no botão finalizar compra
    $('.finalizar-compra').click(function () {
        carrinho.FinalizarCompra();
    });
});
