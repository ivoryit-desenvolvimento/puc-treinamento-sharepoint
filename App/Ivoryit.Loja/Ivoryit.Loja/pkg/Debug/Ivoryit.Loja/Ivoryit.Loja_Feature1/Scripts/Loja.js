﻿'use strict';

function Loja() {
    this.SPContext = null;
    this.SPListaLojas = null;


    /**
     * Função construtora.
     * @access Private
     * @return Void
     */
    this.Construtor = function () {
        var spAppWebUrl = app.ObterUrlParametro('SPAppWebUrl');
        var spHostUrl = app.ObterUrlParametro('SPHostUrl');

        // Contexto do aplicativo.
        this.SPContext = new SP.ClientContext(spAppWebUrl);
        var spProxyFactory = new SP.ProxyWebRequestExecutorFactory(spAppWebUrl);
        this.SPContext.set_webRequestExecutorFactory(spProxyFactory);

        // Contexto do site externo.
        var spContextSite = new SP.AppContextSite(this.SPContext, spHostUrl);

        // Selecione a lista do site externo.
        this.SPListaLojas = spContextSite.get_web().get_lists().getByTitle('Lojas');
    }

    /**
     * Obtem todos os pedidos do usuário logado.
     * @access Private
     * @return Void
     */
    this.CarregarGrid = function () {
        // Monta query que obtem todas as lojas cadastradas.
        var query = new CamlBuilder()
            .View(['Title', 'Informacoes'])
            .Query().Where().All()
            .ToString();

        var camlQuery = new SP.CamlQuery();
        camlQuery.set_viewXml(query);

        var lojas = this.SPListaLojas.getItems(camlQuery);
        this.SPContext.load(lojas);

        this.SPContext.executeQueryAsync(
            function () {
                var listItemEnumerator = lojas.getEnumerator();
                while (listItemEnumerator.moveNext()) {
                    var item = listItemEnumerator.get_current();

                    var linhaLoja =
                        '<tr>' +
                            '<td>' + item.get_item('Title') + '</td>' +
                            '<td>' + item.get_item('Informacoes') + '</td>' +
                        '</tr>';

                    $('#lojas-grid').append(linhaLoja);
                }
            },
            function (sender, args) {
                console.error(args.get_message() + '\n' + args.get_stackTrace());
            }
        );
    }


    this.Construtor();
}

$(document).ready(function () {
    var loja = new Loja()
    loja.CarregarGrid();
});
