﻿<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="../_catalogs/masterpage/Loja.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Valor</th>
                <th>#</th>
            </tr>
        </thead>
        <tbody id="produtos-grid">
        </tbody>
    </table>

    <div id="produtos-paginacao">
        <button class="anterior btn">Anterior</button>
        <span class="indice btn"></span>
        <button class="proximo btn">Próximo</button>
    </div>
</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceHolderScripts" runat="server">
    <script type="text/javascript" src="../Scripts/Produto.js"></script>
</asp:Content>
