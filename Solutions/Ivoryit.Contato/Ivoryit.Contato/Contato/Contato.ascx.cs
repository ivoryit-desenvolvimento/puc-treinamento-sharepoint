﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;

namespace Ivoryit.Contato.Contato
{
    [ToolboxItemAttribute(false)]
    public partial class Contato : WebPart
    {
        private string MensagemId
        {
            get
            {
                try
                {
                    var mensagemId = Page.Request.QueryString["MensagemId"];
                    if (string.IsNullOrEmpty(mensagemId))
                    {
                        return null;
                    }
                    return mensagemId;
                }
                catch
                {
                    return null;
                }
            }
        }
        private string EmailResponder;


        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public Contato()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Formulário em modo de resposta.
            if (!string.IsNullOrEmpty(MensagemId))
            {
                // Obtem os dados da mensagem a ser respondida e os inseri na tela. 
                // E exibe o painel com a mensagem a ser respondida.
                var mensagemResponder = ObterMensagemResponder();
                MensagemResponderTitulo.InnerText = mensagemResponder["Title"].ToString();
                MensagemResponderMensagem.InnerText = mensagemResponder["Mensagem"].ToString();
                MensagemResponder.Visible = true;

                EmailResponder = mensagemResponder["Email"].ToString();

                // Altera o título formulário.
                FormularioTitulo.InnerText = "Responder";
            }
        }

        protected SPListItem ObterMensagemResponder()
        {
            using (var site = new SPSite(SPContext.Current.Site.ID))
            {
                using (var web = site.OpenWeb(SPContext.Current.Web.ID))
                {
                    // Seleciona a lista.
                    var lista = web.Lists.TryGetList("Contato");
                    // Obtem mensagem a ser respondida.
                    return lista.GetItemById(Convert.ToInt32(MensagemId));
                }
            }
        }

        protected void Enviar_Click(object sender, EventArgs e)
        {
            using (var site = new SPSite(SPContext.Current.Site.ID))
            {
                using (var web = site.OpenWeb(SPContext.Current.Web.ID))
                {
                    // Seleciona a lista.
                    var lista = web.Lists["Contato"];

                    // Cadastra um novo contato.
                    var item = lista.Items.Add();
                    item["Title"] = Titulo.Text;
                    item["Email"] = Email.Text;
                    item["Mensagem"] = Mensagem.Text;
                    item["Referencia"] = MensagemId;
                    item.Update();

                    // Formulário nova mensagem
                    if (string.IsNullOrEmpty(MensagemId))
                    {
                        EnviarEmail(
                            web,
                            "treinamento@treinamento.com",
                            "thiago.resende@ivoryit.com.br",
                            "Nova Mensagem",
                            "<a href=\"http://treinamento/Paginas/Contato.aspx?MensagemId=" + item .ID + "\">Responder mensagem</a>");
                    }
                    else
                    {
                        EnviarEmail(
                            web,
                            "treinamento@treinamento.com",
                            EmailResponder,
                            "RES: Nova Mensagem",
                            "reposta da mensagem");
                    }

                    // Exibe mensagem de mensagem enviada com sucesso e atualiza a página. 
                    // Para que a web part para limpar o formulário
                    Page.ClientScript.RegisterClientScriptBlock(
                        GetType(),
                        "AtualizarPagina",
                        "alert('Tarefa cadastrada com sucesso.'); window.location.href=window.location.href;",
                        true);
                }
            }
        }

        protected void EnviarEmail(
            SPWeb spWeb,
            string remetente,
            string destinatario,
            string titulo,
            string mensagem
        )
        {
            var mensagemCabecalho = new StringDictionary();
            mensagemCabecalho.Add("from", remetente);
            mensagemCabecalho.Add("to", destinatario);
            mensagemCabecalho.Add("subject", titulo);
            mensagemCabecalho.Add("content-type", "text/html");

            var enviarEmail = SPUtility.SendEmail(
                spWeb,
                mensagemCabecalho,
                mensagem
            );
        }
    }
}
