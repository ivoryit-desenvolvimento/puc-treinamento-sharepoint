﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Contato.ascx.cs" Inherits="Ivoryit.Contato.Contato.Contato" %>

<div id="MensagemResponder" runat="server" visible="false">
    <h1>Mensagem</h1>

    Título: <br>
    <div id="MensagemResponderTitulo" runat="server"></div>

    Mensagem: <br>
    <div id="MensagemResponderMensagem" runat="server"></div>
</div>

<h1 id="FormularioTitulo" runat="server">Nova mensagem</h1>
<p>
    <asp:Label AssociatedControlID="Titulo" runat="server">Título</asp:Label>
    <br>
    <asp:TextBox ID="Titulo" runat="server"></asp:TextBox>
</p>

<p>
    <asp:Label AssociatedControlID="Titulo" runat="server">E-mail</asp:Label>
    <br>
    <asp:TextBox ID="Email" runat="server"></asp:TextBox>
</p>

<p>
    <asp:Label AssociatedControlID="Mensagem" runat="server">Mensagem</asp:Label>
    <br>
    <asp:TextBox ID="Mensagem" runat="server"></asp:TextBox>
</p>

<p>
    <asp:Button ID="Enviar" Text="Enviar" OnClick="Enviar_Click" runat="server" />
</p>