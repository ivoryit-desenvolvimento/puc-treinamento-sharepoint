﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Linq;
using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using System.Linq;

namespace Ivoryit.Tarefas.Lista.RelatorioTarefasAbertas
{
    [ToolboxItemAttribute(false)]
    public partial class RelatorioTarefasAbertas : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public RelatorioTarefasAbertas()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ExibirTarefas();
            }
        }

        protected void ExibirTarefas()
        {
            // Obtem tarefas com Linq
            var tarefas = ObterTarefasComLinq();
            TarefaLista.DataSource = tarefas;

            // Obtem tarefas com Caml Query
            //var tarefas = ObterTarefasComCaml();
            //var campos = tarefas.Fields;
            //TarefaLista.DataSource = tarefas.GetDataTable();

            TarefaLista.DataBind();
        }

        protected object ObterTarefasComLinq()
        {
            using (var contexto = new DataContext(SPContext.Current.Web.Url))
            {
                var tarefas = contexto.GetList<TarefasItem>("Tarefas");

                var tarefasResultado =
                    from tarefa in tarefas
                    where tarefa.Status.ToString() == "Aberta"
                    orderby tarefa.Título
                    select new
                    {
                        ID = tarefa.ID,
                        Title = tarefa.Título,
                        Status = tarefa.Status,
                        Responsavel = tarefa.Responsavel.Título,
                        ResponsavelEmail = tarefa.Responsavel.Email
                    };

                // Exemplo utilizando expressão lambda 
                //var tarefasResultado = tarefas
                //    .Where(tarefa => tarefa.Status.ToString() == "Aberta")
                //    .OrderBy(tarefa => tarefa.Título)
                //    .Select(tarefa => new
                //    {
                //        ID = tarefa.ID,
                //        Title = tarefa.Título,
                //        Status = tarefa.Status,
                //        Responsavel = tarefa.Responsavel.Título,
                //        ResponsavelEmail = tarefa.Responsavel.Email
                //    });

                return tarefasResultado;
            }
        }

        protected SPListItemCollection ObterTarefasComCaml()
        {
            using (var site = new SPSite(SPContext.Current.Site.ID))
            {
                using (var web = site.OpenWeb(SPContext.Current.Web.ID))
                {
                    // Seleciona a lista.
                    var lista = web.Lists.TryGetList("Tarefas");

                    // Monta a consulta.
                    var query = new SPQuery();
                    query.ViewFields = @"
                        <FieldRef Name='Title' />
                        <FieldRef Name='Status' />
                        <FieldRef Name='Responsavel' />
                        <FieldRef Name='ResponsavelEmail' />";
                    query.Joins = @"
                        <Join Type='LEFT' ListAlias='TarefasResponsaveis'>
                            <Eq>
                                <FieldRef Name='Responsavel' RefType='ID' />
                                <FieldRef Name='ID' List='TarefasResponsaveis' />
                            </Eq>
                        </Join>";
                    query.ProjectedFields = @"
                        <Field ShowField='Email' Type='Lookup' Name='ResponsavelEmail' 
                            List='TarefasResponsaveis' />";
                    query.Query = @"
                        <OrderBy>
                            <FieldRef Name='Title' />
                        </OrderBy>
                        <Where>
                            <Eq>
                                <FieldRef Name='Status' />
                                <Value Type='Choice'>Aberta</Value>
                            </Eq>
                        </Where>";

                    // Obtem os itens.
                    return lista.GetItems(query);
                }
            }
        }
    }
}
