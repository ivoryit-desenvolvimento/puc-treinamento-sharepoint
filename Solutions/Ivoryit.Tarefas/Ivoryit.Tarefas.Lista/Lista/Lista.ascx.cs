﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;

namespace Ivoryit.Tarefas.Lista.Lista
{
    [ToolboxItemAttribute(false)]
    public partial class Lista : WebPart
    {
        public enum TarefasStatus
        {
            Todos,
            Aberta,
            Finalizada
        };

        protected TarefasStatus _FiltroStatus;
        
        [WebBrowsable(true),
         WebDisplayName("Obter tarefas com o status"),
         WebDescription("Filtra as tarefas a serem exibidas por status."),
         Personalizable(PersonalizationScope.Shared),
         Category("Filtro")]
        public TarefasStatus FiltroStatus
        {
            get { return _FiltroStatus; }
            set { _FiltroStatus = value; }
        }

        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public Lista()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ExibirTarefas();
            }
        }

        protected void ExibirTarefas()
        {
            //teste.InnerText = ConfiguracaoStatus;
            // Obtem tarefas cadastradas.
            var tarefas = ObterTarefas();

            // Repassa as informações para o grid.
            TarefaLista.DataSource = tarefas.GetDataTable();
            TarefaLista.DataBind();
        }

        protected SPListItemCollection ObterTarefas()
        {
            using (var site = new SPSite(SPContext.Current.Site.ID))
            {
                using (var web = site.OpenWeb(SPContext.Current.Web.ID))
                {
                    // Seleciona a lista.
                    var lista = web.Lists.TryGetList("Tarefas");

                    // De acordo com a configuração da web part. Filtra as tarefas pelos status.
                    if (string.IsNullOrEmpty(FiltroStatus.ToString()) || FiltroStatus.ToString() == "Todos")
                    {
                        return lista.GetItems();
                    }
                    else
                    {
                        var query = new SPQuery();
                        query.Query = string.Format(@"
                            <Where>
                                <Eq>
                                    <FieldRef Name='Status' />
                                    <Value Type='Choice'>{0}</Value>
                                </Eq>
                            </Where>", FiltroStatus.ToString());

                        return lista.GetItems(query);
                    }
                }
            }
        }

        protected void FinalizarTarefa(int id)
        {
            using (var site = new SPSite(SPContext.Current.Site.ID))
            {
                using (var web = site.OpenWeb(SPContext.Current.Web.ID))
                {
                    // Seleciona a lista.
                    var lista = web.Lists.TryGetList("Tarefas");

                    // Atualiza o status da tarefa.
                    var tarefa = lista.Items.GetItemById(id);
                    tarefa["Status"] = "Finalizada";
                    tarefa.Update();
                }
            }
        }

        protected void ExcluirTarefa(int id)
        {
            using (var site = new SPSite(SPContext.Current.Site.ID))
            {
                using (var web = site.OpenWeb(SPContext.Current.Web.ID))
                {
                    // Seleciona a lista.
                    var lista = web.Lists.TryGetList("Tarefas");

                    // Exclui tarefa.
                    lista.Items.DeleteItemById(id);
                }
            }
        }

        protected void TarefaLista_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
        {
            var linha = Convert.ToInt32(e.CommandArgument);
            var id = Convert.ToInt32(TarefaLista.DataKeys[linha].Value.ToString());

            if (e.CommandName == "FinalizarTarefa")
            {
                FinalizarTarefa(id);
            }
            else if (e.CommandName == "ExcluirTarefa")
            {
                ExcluirTarefa(id);
            }

            ExibirTarefas();
        }
    }
}
