﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Lista.ascx.cs" Inherits="Ivoryit.Tarefas.Lista.Lista.Lista" %>

<asp:GridView 
    ID="TarefaLista" 
    DataKeyNames="ID" 
    AutoGenerateColumns="false" 
    OnRowCommand="TarefaLista_RowCommand" 
    EmptyDataText="Nenhuma tarefa cadastrada."
    runat="server">
    <Columns>
        <asp:BoundField DataField="Title" HeaderText="Tarefa" />
        <asp:BoundField DataField="Status" HeaderText="Status" />
        <asp:ButtonField ButtonType="Button" Text="Finalizar" CommandName="FinalizarTarefa"  />
        <asp:ButtonField ButtonType="Button" Text="Excluir" CommandName="ExcluirTarefa"  />
    </Columns>
</asp:GridView>
