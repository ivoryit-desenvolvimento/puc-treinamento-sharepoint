﻿using Microsoft.SharePoint;
using System;
using System.ComponentModel;
using System.IO;
using System.Web.UI.WebControls.WebParts;

namespace Ivoryit.Tarefas.Formulario.Formulario
{
    [ToolboxItemAttribute(false)]
    public partial class Formulario : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public Formulario()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void SalvarTarefa(object sender, EventArgs e)
        {
            using (var site = new SPSite(SPContext.Current.Site.ID))
            {
                using (var web = site.OpenWeb(SPContext.Current.Web.ID))
                {
                    // Seleciona a lista.
                    var lista = web.Lists["Tarefas"];

                    // Cadastra um novo item (tarefa).
                    var item = lista.Items.Add();
                    item["Title"] = TarefaTitulo.Text;
                    item.Update();

                    // Registra no log o cadastro de uma nova tarefa.
                    RegistrarLog(TarefaTitulo.Text);

                    // Exibe mensagem de cadastro realizado com sucesso e atualiza a página. 
                    // Para que a web part de listagem obtenha os dados novamente.
                    Page.ClientScript.RegisterClientScriptBlock(
                        GetType(), 
                        "AtualizarPagina", 
                        "alert('Tarefa cadastrada com sucesso.'); window.location.href=window.location.href;", 
                        true);
                }
            }
        }

        protected void RegistrarLog(string tarefa)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                string arquivo = @"C:\_Projetos\LogTarefas.txt";
                using (var sw = File.AppendText(arquivo))
                {
                    var data = String.Format("{0:dd/MM/yyyy HH:mm:ss}", DateTime.Now);
                    sw.WriteLine("[{0}] Nova tarefa: {1}", data, tarefa);
                }
            });
        }
    }
}
