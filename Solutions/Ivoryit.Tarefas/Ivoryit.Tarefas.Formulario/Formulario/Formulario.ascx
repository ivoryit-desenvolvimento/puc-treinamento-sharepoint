﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Formulario.ascx.cs" Inherits="Ivoryit.Tarefas.Formulario.Formulario.Formulario" %>

<link rel="stylesheet" href="/_layouts/15/Ivoryit.Tarefas.Formulario/style.css" />

<div>
    <asp:Label AssociatedControlID="TarefaTitulo" runat="server">Tarefa:</asp:Label>
    <asp:TextBox ID="TarefaTitulo" runat="server" ></asp:TextBox>
    <asp:RequiredFieldValidator 
        ID="TarefaTituloValidacao"
        ControlToValidate="TarefaTitulo" 
        EnableClientScript="true" 
        ErrorMessage="Este campo é obrigatório."
        Display="Static"
        runat="server">
    </asp:RequiredFieldValidator>
</div>
<div>
    <asp:Button ID="TarefaSalvar" OnClick="SalvarTarefa" runat="server" Text="Salvar" />
</div>