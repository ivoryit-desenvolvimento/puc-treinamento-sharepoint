﻿using Microsoft.Office.Server.Search;
using Microsoft.Office.Server.Search.Query;
using Microsoft.SharePoint;
using System;
using System.ComponentModel;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;

namespace Ivoryit.Busca.Busca
{
    [ToolboxItemAttribute(false)]
    public partial class Busca : WebPart
    {
        private int ItensPorPagina = 2;
        private int Pagina
        {
            set { Pagina = 1; }
            get
            {
                try
                {
                    var pagina = Page.Request.QueryString["Pagina"];
                    var paginaAtual = string.IsNullOrEmpty(pagina) || pagina == "0"
                        ? 1
                        : Convert.ToInt32(pagina);

                    return paginaAtual;
                }
                catch
                {
                    return 1;
                }
            }
        }


        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public Busca()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            using (var site = new SPSite(SPContext.Current.Site.ID))
            {
                // Monta uma nova consulta por palavara chave. Passando o site a qual iremos consultar.
                var keywordQuery = new KeywordQuery(site);
                
                // Condição.
                keywordQuery.QueryText = "size>1";

                // Informa quantos itens queremos por página.
                keywordQuery.RowsPerPage = ItensPorPagina;
                // Quantos intens queremos que retona a consulta atual.
                keywordQuery.RowLimit = ItensPorPagina;
                // Em qual linha deve começar o retorno.
                keywordQuery.StartRow = (Pagina * keywordQuery.RowLimit) - 2;

                // Realiza a consulta.
                var searchExecutor = new SearchExecutor();
                var resultado = searchExecutor.ExecuteQuery(keywordQuery);
                var resultadoTabelas = resultado.Filter("TableType", KnownTableTypes.RelevantResults);
                var resultadoTabela = resultadoTabelas.GetEnumerator();
                resultadoTabela.MoveNext();

                // Páginação
                // Total de páginas.
                decimal totalDePaginas = 
                    resultadoTabela.Current.TotalRows / keywordQuery.RowLimit;
                totalDePaginas = Math.Ceiling(totalDePaginas);

                // Exibe os links de páginação.
                for (var i = 1; i <= totalDePaginas; i++)
                {
                    var pagina = new HtmlAnchor();
                    pagina.InnerText = i.ToString();
                    pagina.HRef = 
                        Page.Request.Url.GetLeftPart(UriPartial.Path).ToString() + 
                        "?pagina=" + i.ToString();

                    Paginacao.Controls.Add(pagina);
                }

                // Exibe os dados no grid.
                GridBusca.DataSource = resultadoTabela.Current.Table;
                GridBusca.DataBind();
            }
        }
    }
}
